
# My BitBucket Portfolio

## Brian Kelly

### FSU Classes Taken:

- #### LIS 3781: Advanced Database Management
- #### LIS 4331: Advanced Mobile Applications Development
- #### LIS 4368: Advanced Web Applications Development



### Repository Course Work Links:

#### LIS 3781: Advanced Database Management Repository:
https://bitbucket.org/bpk16/lis3781/src/master/

#### LIS 4331: Advanced Mobile Applications Development Repository:
https://bitbucket.org/bpk16/lis4331/src/master/

#### LIS 4368: Advanced Web Applications Development Repository:
https://bitbucket.org/bpk16/lis4368/src/master/


